package hw1_2

import java.nio.file.Paths

import org.apache.spark.sql.functions.desc
import org.apache.spark.sql.types.{FloatType, IntegerType, StringType, StructField, StructType}
import org.apache.spark.sql.{DataFrame, SQLContext}
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Homework 1 part 2.
  * Use dataset from Hive_Basics_p1 and Spark SQL:
  * 5.	Dataset http://stat-computing.org/dataexpo/2009/the-data.html (year 2007 + dimensions http://stat-computing.org/dataexpo/2009/supplemental-data.html only Airports and Carrier Codes).
  * 6.	Count total number of flights per carrier in 2007
  * 7.	The total number of flights served in Jun 2007 by NYC (all airports, use join with Airports data). How many stages in jobs where instanced for this query?
  * 8.	Find five most busy airports in US during Jun 01 - Aug 31. How many stages in jobs where instanced for this query?
  * 9.	Find the carrier who served the biggest number of flights
  */
object App {
  def main(args: Array[String]) {
    // parse args
    if (args.length != 2) {
      println("Usage: App <in> <out>")
      sys.exit(-1)
    }

    val in: String = args(0)
    val out: String = args(1)

    // init
    val sc: SparkContext = SparkContext.getOrCreate(new SparkConf().setAppName("hw1_2"))
    val sqlContext: SQLContext = SQLContext.getOrCreate(sc)

    // load input
    val df_flights: DataFrame = sqlContext.read
      .format("com.databricks.spark.csv")
      .option("header", "true") // Use first line of all files as header
      .schema(Schemas.flights)
      .load(Paths.get(in, "2007.csv").toString)

    val df_airports: DataFrame = sqlContext.read
      .format("com.databricks.spark.csv")
      .option("header", "true") // Use first line of all files as header
      .schema(Schemas.airports)
      .load(Paths.get(in, "airports.csv").toString)

    val df_carriers: DataFrame = sqlContext.read
      .format("com.databricks.spark.csv")
      .option("header", "true") // Use first line of all files as header
      .schema(Schemas.carriers)
      .load(Paths.get(in, "carriers.csv").toString)

    df_flights.registerTempTable("flights")
    df_airports.registerTempTable("airports")
    df_carriers.registerTempTable("carriers")

    // task 6
    // Count total number of flights per carrier in 2007
    val result1: DataFrame = df_flights.groupBy("UniqueCarrier").count()

    result1.write
      .format("com.databricks.spark.csv")
      .option("header", "true")
      .save(Paths.get(out, "task6").toString)

    // task 7
    // The total number of flights served in Jun 2007 by NYC (all airports, use join with Airports data).
    val result2: DataFrame = sqlContext.sql(
      """
        |select count(1) as count
        |from flights as f
        |join airports as a
        |on f.Dest = a.iata
        |where f.Month = 6
        |and a.city = 'New York'
      """.stripMargin)
    result2.write
      .format("com.databricks.spark.csv")
      .option("header", "true")
      .save(Paths.get(out, "task7").toString)

    // task 8
    // Find five most busy airports in US during Jun 01 - Aug 31.
    val result3: DataFrame = sqlContext.sql(
      """
        |with
        |filtered as (
        |	select Origin, Dest
        |	from flights
        |	where Month >= 6
        |	and Month <= 8
        |	),
        |iatas as (
        |	select Origin iata
        |	from filtered
        |	union all
        |	select Dest iata
        |	from filtered
        |	)
        |select iata, count(iata) count
        |from iatas
        |group by iata
        |order by count desc
        |limit 5
      """.stripMargin
    )

    result3.write
      .format("com.databricks.spark.csv")
      .option("header", "true")
      .save(Paths.get(out, "task8").toString)

    // task 9
    // Find the carrier who served the biggest number of flights
    val result4 = result1.orderBy(desc("count")).limit(1)
    result4.write
      .format("com.databricks.spark.csv")
      .option("header", "true")
      .save(Paths.get(out, "task9").toString)
  }
}

object Schemas {
  val flights = StructType(
    Seq(
      StructField("Year", IntegerType),
      StructField("Month", IntegerType),
      StructField("DayofMonth", IntegerType),
      StructField("DayOfWeek", IntegerType),
      StructField("DepTime", IntegerType),
      StructField("CRSDepTime", IntegerType),
      StructField("ArrTime", IntegerType),
      StructField("CRSArrTime", IntegerType),
      StructField("UniqueCarrier", StringType),
      StructField("FlightNum", IntegerType),
      StructField("TailNum", StringType),
      StructField("ActualElapsedTime", IntegerType),
      StructField("CRSElapsedTime", IntegerType),
      StructField("AirTime", IntegerType),
      StructField("ArrDelay", IntegerType),
      StructField("DepDelay", IntegerType),
      StructField("Origin", StringType),
      StructField("Dest", StringType),
      StructField("Distance", IntegerType),
      StructField("TaxiIn", IntegerType),
      StructField("TaxiOut", IntegerType),
      StructField("Cancelled", IntegerType),
      StructField("CancellationCode", IntegerType),
      StructField("Diverted", IntegerType),
      StructField("CarrierDelay", IntegerType),
      StructField("WeatherDelay", IntegerType),
      StructField("NASDelay", IntegerType),
      StructField("SecurityDelay", IntegerType),
      StructField("LateAircraftDelay", IntegerType)
    )
  )

  val airports = StructType(
    Seq(
      StructField("iata", StringType),
      StructField("airport", StringType),
      StructField("city", StringType),
      StructField("state", StringType),
      StructField("country", StringType),
      StructField("lat", FloatType),
      StructField("long", FloatType)
    )
  )

  val carriers = StructType(
    Seq(
      StructField("Code", StringType),
      StructField("Description", StringType)
    )
  )
}
