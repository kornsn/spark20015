package hw1_1

/**
  * Created by Sergei_Korneev on 2015-12-10.
  */
object ParserSandbox {
  def main(args: Array[String]) {
    val x = "ip1 - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 40028 \"-\" \"Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\""
//    val x = "ip30 - - [24/Apr/2011:05:42:32 -0400] \"GET / HTTP/1.1\" 200 12550 \"-\" \"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729)\""

    val in = List(x)
    in.flatMap(parse).foreach(println)
  }

  val reg = """^(?<ip>\w+) \S+ \S+ \[.*\] ".*" \d+ (?<bytes>\d+) "-" "(?<agent>.*)"""".r

  def parse(string: String) = {
    reg
      .findAllIn(string)
      .matchData
      .map(x =>
        (x.group(1), x.group(2).toInt, Utils.parseUserAgent(x.group(3))))
      .toArray
  }
}
