package hw1_1

import net.sf.uadetector.ReadableUserAgent
import net.sf.uadetector.service.UADetectorServiceFactory

/**
  * Created by Sergei_Korneev on 2015-12-09.
  */
object Utils {
  val parser = UADetectorServiceFactory.getCachingAndUpdatingParser

  def parseUserAgent(string: String): String = {
    val agent: ReadableUserAgent = parser.parse(string)
    agent.getFamily.getName
  }
}
