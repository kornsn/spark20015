package hw1_1

import org.apache.spark.{SparkConf, SparkContext}


/**
  * Created by Sergei_Korneev on 2015-12-08.
  */
object Sandbox {
  val NUM_SAMPLES = 1000000

  def main(args: Array[String]) {
    val sc = new SparkContext(new SparkConf().setAppName("HelloKitty"))
    val count = sc.parallelize(1 to NUM_SAMPLES).map { i =>
      val x = Math.random()
      val y = Math.random()
      if (x * x + y * y < 1) 1 else 0
    }.reduce(_ + _)
    println("Pi is roughly " + 4.0 * count / NUM_SAMPLES)
  }

}
