package hw1_1

import org.apache.spark.rdd.RDD
import org.apache.spark.{Accumulator, SparkConf, SparkContext}

/**
  * Homework 1 part 1:
  * Use dataset from Hive_Basics_p3 and Spark:
  * 1.	Dataset (web-server access log): https://kb.epam.com/download/attachments/241634481/access_logs.rar
  * 2.	Count average bytes per request by IP and total bytes by IP; output is CSV file with rows as next:
  * IP,175.5,109854
  * 3.	Add Spark Unit tests for your Job
  * 4.	Use accumulators to get stats how many users of IE, Mozzila or Other were detected (parse it from UserAgend: ip13 - - [24/Apr/2011:04:41:53 -0400] "GET /logs/access_log.3 HTTP/1.1" 200 4846545 "-" "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)") and print them in STDOUT of Driver
  */
object App {

  /**
    * Entry point.
    * @param args Command line arguments.
    */
  def main(args: Array[String]) {
    if (args.length != 2) {
      println(s"Usage: $getClass <in> <out>")
      sys.exit(-1)
    }

    // parse command line args
    val in = args(0)
    val out = args(1)

    // init
    val sc: SparkContext = SparkContext.getOrCreate(new SparkConf().setAppName("HelloKitty"))

    val textFile: RDD[String] = sc.textFile(in)

    // parse input
    val input: RDD[(String, Int, String)] = textFile.flatMap(parse_input)

    // process input
    val result = process(input)

    // init accumulators
    val firefoxAcc: Accumulator[Int] = sc.accumulator(0, "firefox")
    val ieAcc: Accumulator[Int] = sc.accumulator(0, "ie")
    val otherAcc: Accumulator[Int] = sc.accumulator(0, "other")

    // increase accumulators and format output
    result.map(x => {
      if (x.firefox_detected) {
        firefoxAcc += 1
      }
      if (x.ie_detected) {
        ieAcc += 1
      }
      if (x.other_detected) {
        otherAcc += 1
      }

      // Format output
      s"${x.ip},${x.avg},${x.total}"
    })
      // save to file
      .saveAsTextFile(out)

    // Print accumulators values.
    println(s"firefox: ${firefoxAcc.value}")
    println(s"ie: ${ieAcc.value}")
    println(s"others: ${otherAcc.value}")
  }


  /**
    * Regexp for parsing input line.
    */
  val reg =
    """^(?<ip>\w+) \S+ \S+ \[.*\] ".*" \d+ (?<bytes>\d+) ".*" "(?<agent>.*)"""".r

  /**
    * Parses log string.
    * @param string Log string.
    * @return Tuple in form of (ip, bytes_count, user_agent).
    */
  def parse_input(string: String): Array[(String, Int, String)] = {
    reg
      .findAllIn(string)
      .matchData
      .map(x =>
        (x.group(1), x.group(2).toInt, Utils.parseUserAgent(x.group(3))))
      .toArray
  }

  /**
    * Main method for processing whole rdd.
    * @param input Input rdd.
    * @return Result rdd.
    */
  def process(input: RDD[(String, Int, String)]): RDD[Result] = {
    input
      .groupBy(_._1) // group by ip
      .map(x => {
      val g = processGroup(x._2)

      new Result(x._1, (g._1: Float) / g._2, g._1, g._3 != 0, g._4 != 0, g._5 != 0)
    })
  }

  /**
    * Processes one group of logs grouped by ip.
    * @param g Group of tuple(ip, byte_count, user_agent).
    * @return Tuple(sum_of_byte_count, count, Firefox_detected, IE_detected, Other_detected).
    */
  def processGroup(g: Iterable[(String, Int, String)]): (Int, Int, Int, Int, Int) = {
    // calc sum, count, firefox queries, ie queries, other queries
    g.foldLeft((0, 0, 0, 0, 0))(
      (a, x) => {
        var f = a._3
        var ie = a._4
        var o = a._5
        x._3 match {
          case "Firefox" => f = 1
          case "IE" => ie = 1
          case _ => o = 1
        }
        (a._1 + x._2, a._2 + 1, f, ie, o)
      }
    )
  }
}

/**
  * Represents result of processing.
  * @param ip Ip.
  * @param avg Average amount of bytes per request.
  * @param total Total amount of bytes..
  * @param firefox_detected Indicates that user with this ip used Firefox agent at least once.
  * @param ie_detected Indicates that user with this ip used IE agent at least once.
  * @param other_detected Indicates that user with this ip used Other agent at least once.
  */
class Result(
              val ip: String,
              val avg: Float,
              val total: Long,
              val firefox_detected: Boolean,
              val ie_detected: Boolean,
              val other_detected: Boolean
            )
  extends java.io.Serializable {

  def canEqual(other: Any): Boolean = other.isInstanceOf[Result]

  override def equals(other: Any): Boolean = other match {
    case that: Result =>
      (that canEqual this) &&
        ip == that.ip &&
        avg == that.avg &&
        total == that.total &&
        firefox_detected == that.firefox_detected &&
        ie_detected == that.ie_detected &&
        other_detected == that.other_detected
    case _ => false
  }

  override def hashCode(): Int = {
    val state = Seq(ip, avg, total, firefox_detected, ie_detected, other_detected)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }

  override def toString = s"Result(ip=$ip, avg=$avg, total=$total, firefox_detected=$firefox_detected, ie_detected=$ie_detected, other_detected=$other_detected)"
}
