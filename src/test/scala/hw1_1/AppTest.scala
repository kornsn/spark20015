package hw1_1

import org.apache.spark.SparkContext
import org.scalatest.{BeforeAndAfter, FunSuite}

/**
  * Created by Sergei_Korneev on 2015-12-15.
  */
class AppTest extends FunSuite with BeforeAndAfter {
  var sc: SparkContext = _

  before {
    System.clearProperty("spark.driver.port")
    System.clearProperty("spark.hostPort")

    sc = new SparkContext("local", getClass.getSimpleName)
  }

  after {
    sc.stop()
    sc = null

    System.clearProperty("spark.driver.port")
    System.clearProperty("spark.hostPort")
  }

  test("test process group") {
    val in = Seq(
      ("ip1", 1, "Firefox"),
      ("ip1", 2, "Firefox"),
      ("ip1", 3, "IE"),
      ("ip1", 4, "Opera"),
      ("ip1", 5, "Firefox"),
      ("ip1", 6, "Firefox")
    )
    val expected = (21, 6, 1, 1, 1)
    val result = App.processGroup(in)
    assert(result === expected)
  }

  test("test process group2") {
    val in = Seq(
      ("ip1", 1, "Firefox"),
      ("ip1", 2, "Firefox"),
      ("ip1", 3, "IE"),
      ("ip1", 4, "IE"),
      ("ip1", 5, "Firefox"),
      ("ip1", 6, "Firefox")
    )
    val expected = (21, 6, 1, 1, 0)
    val result = App.processGroup(in)
    assert(result === expected)
  }

  test("Test process") {
    val in = sc.makeRDD(Seq(
      ("ip1", 1, "Firefox"),
      ("ip1", 2, "IE"),
      ("ip1", 6, "Opera"),
      ("ip2", 5, "Firefox"),
      ("ip2", 6, "Firefox"),
      ("ip2", 10, "Firefox")
    ))
    val expected = Array(
      new Result("ip1", 3.0f, 9, true, true, true),
      new Result("ip2", 7.0f, 21, true, false, false)
    )
    val result = App.process(in)
    assert(result.collect().toSet === expected.toSet)
  }
}

class UtilsTest extends FunSuite {
  test("agent parsing") {
    val in = """Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)"""
    val expected = "YandexBot"
    val result = Utils.parseUserAgent(in)
    assert(result === expected)
  }
  test("agent parsing unknown") {
    val in = """bla-bla"""
    val expected = "unknown"
    val result = Utils.parseUserAgent(in)
    assert(result === expected)
  }

  test("test input parsing") {
    val in = """ip1 - - [24/Apr/2011:04:06:01 -0400] "GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1" 200 40028 "-" "Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)""""
    val expected = Array(("ip1", 40028, "YandexBot"))
    val result = App.parse_input(in)
    assert(result === expected)
  }

  test("test input parsing empty on wrong input") {
    {
      val in = """bla-bla""""
      val expected = Array()
      val result = App.parse_input(in)
      assert(result === expected)
    }
  }
}
